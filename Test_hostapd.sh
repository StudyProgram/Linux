#!/bin/bash
set -Cu

echo -n "access point(hostapd)のInstallをしますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	sudo apt-get install hostapd

	sudo cp /usr/share/doc/hostapd/examples/hostapd.conf.gz /etc/hostapd/
	sudo gzip -d /etc/hostapd/hostapd.conf.gz
	sudo apt-get install bridge-utils -y
	cd /etc/hostapd

	echo "無線Lan設定"
	A="interface=wlan0" 
	B="interface=wlan0-ap"
	echo "$A -> $B"　　
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "bridge設定"
	A="#bridge=br0"
	B="bridge=br0"
	echo "$A -> $B"　　
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf


	echo "認証アルゴリズム設定"
	A="auth_algs=3" 
	B="auth_algs=1"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "チャンネル設定"
	A="channel=1" 
	B="channel=6"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "ドライバ設定"
	A="# driver=hostap" 
	B="driver=nl80211\n#rtl871xdrv"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf


	echo "wpa設定"
	A="#wpa=1" 
	B="wpa=2"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "ssid設定"
	A="ssid=test" 
	B="ssid=RaspberryPI3"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "pass phrase設定"
	read INPUT_WORD
	A="#wpa_passphrase=.*" 
	B="wpa_passphrase=$INPUT_WORD"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "鍵管理アルゴリズム設定"
	A="#wpa_key_mgmt=WPA-PSK WPA-EAP" 
	B="wpa_key_mgmt=WPA-PSK"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "WPA Pair wise"
	A="#wpa_pairwise=TKIP CCMP" 
	B="wpa_pairwise=TKIP"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "RSN/WPAの共有鍵暗号化方式"
	A="#rsn_pairwise=CCMP" 
	B="rsn_pairwise=CCMP"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "IEEE 802.11n を有効"
	A="#ieee80211n=1"
	B="ieee80211n=1"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "ht_capab"
	A="#ht_capab=\[HT40-\]\[SHORT-GI-20\]\[SHORT-GI-40\]"
	B="ht_capab=[SHORT-GI-20]"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "Country code設定"
	A="#country_code=US"
	B="country_code=JP"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "/etc/default/hostapdの読込みファイル設定"
	A='#DAEMON_CONF=""'
	B='DAEMON_CONF="/etc/hostapd/hostapd.conf"'
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/default/hostapd

	echo "/etc/dhcpcd.confの読込みファイル設定"
	A='#fallback static_eth0'
	B="# fallback static_eth0\nauto br0\n    iface br0 inet dhcp\n    bridge_ports wlan0-ap"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/dhcpcd.conf

fi

echo -n "iptablesを設定しますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then

	echo "iptable install"
	sudo apt-get install iptables-persistent

	sudo sed -i -e 's!#net.ipv4.ip_forward=1!net.ipv4.ip_forward=1!' /etc/sysctl.conf

	echo "iptable 設定"
	sudo iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
	#sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
	sudo iptables -t nat -A POSTROUTING -s 192.168.10.0/24 ! -d 192.168.10.0/24 -j MASQUERADE
	sudo iptables -A FORWARD -i wlan0 -o wlan0-ap -m state --state RELATED,ESTABLISHED -j ACCEPT
	sudo iptables -A FORWARD -i wlan0-ap -o wlan0 -j ACCEPT
	sudo sh -c "iptables-save > /etc/iptables/ipv4.nat"
	sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
	#sudo sed -i -e 's!0!1!' /proc/sys/net/ipv4/ip_forward
fi


echo -n "DNS server(dnsmasq)のInstallをしますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	sudo apt-get install dnsmasq
	sudo apt-get install dnsutils
	sudo apt-get install sysv-rc-conf 
	sudo service dnsmasq stop
	sudo cp /etc/dnsmasq.conf /etc/dnsmasq.conf.old

	echo "機器使用設定"
	A="#interface="
	B="interface=lo,wlan0-ap"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/dnsmasq.conf

	echo "機器未使用設定"
	A="#no-dhcp-interface="
	B="no-dhcp-interface=lo,wlan0"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/dnsmasq.conf

	echo "domain-needed設定"
	A="#domain-needed"
	B="domain-needed"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/dnsmasq.conf

	echo "dhcp-range設定"
	A="#dhcp-range=192.168.0.50,192.168.0.150,12h"
	B="dhcp-range=192.168.0.50,192.168.0.150,24h"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/dnsmasq.conf


	sudo sed -i -e 's!#bogus-priv!bogus-priv!' /etc/dnsmasq.conf
	sudo sed -i -e 's!#local=/localnet/!local=/localnet/!' /etc/dnsmasq.conf
	sudo sed -i -e 's!#expand-hosts!expand-hosts!' /etc/dnsmasq.conf
	sudo sed -i -e 's!#domain=thekelleys.org.uk!domain=thekelleys.org.uk!' /etc/dnsmasq.conf



	echo -n "下記例のように書いてください "
	echo "127.0.0.1     localhost"
	echo "127.0.1.1     raspberrypi"
	echo ""
	echo "192.168.19.2  HOST1"
	echo "192.168.19.3  HOST2"

	#read INPUT_WORD

	#sudo nano /etc/hosts
	sudo service dnsmasq restart
	sudo sysv-rc-conf dnsmasq on
fi

echo -n "access point(hostapd)を起動しますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	#sudo ifconfig wlan0 down
	#sudo ifconfig wlan0 up
	echo "execute hostapd".
	sudo hostapd /etc/hostapd/hostapd.conf
	sudo systemctl status hostapd
fi
echo -n "AccesspointをTestしますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then

	sudo iw dev wlan0 interface add wlan0-ap type __ap
	sudo ifconfig wlan0 down
	sudo ifconfig wlan0 up	
	sudo hostapd /etc/hostapd/hostapd.conf
	sudo /etc/init.d/samba restart
fi

echo -n "webmin sambaのInstallをしますか？(y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	#echo -n "UserNameを入力してください。"
	#read UserName
	echo -n "Passwordを入力してください。"
	read Password
	sudo apt-get install perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions
	wget http://prdownloads.sourceforge.net/webadmin/webmin_1.870_all.deb
	sudo dpkg --install webmin_1.870_all.deb
	sudo iptables -I INPUT -p tcp --dport 10000 -j ACCEPT
	sudo /usr/share/webmin/changepass.pl /etc/webmin root $Password
	ifconfig
	echo -n "IPを入力してください。"
	read IP
	/usr/lib/chromium-browser/chromium-browser https://$IP:10000

	service iptables save
	service iptables restart
	sudo service samba-ad-dc start
fi
echo -n "USB HDDのMount設定をしますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then

	cd /etc
	sudo cp fstab fstab.bak

	echo "mount設定"
	A="#   use  dphys-swapfile swap\[on.\+" 
	B="# use dphys-swapfile swap[on|off] for that\n/dev/sda1  /mnt/USB1               fat32    defaults  0       0"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/fstab
	sudo blkid /dev/sda1
	#sudo nano /etc/fstab
fi