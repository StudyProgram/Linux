#!/bin/bash

set -Cu

echo -n "Sambaを起動しますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	sudo /etc/init.d/samba restart
	sudo service samba status 
fi

echo -n "Onedriveを起動しますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	cd ./ProgramInstall/onedrived-dev
	pwd
	onedrived start --debug
fi

echo -n "Accesspointを起動しますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then

	#sudo killall wpa_supplicant
	#wpa_supplicant -B -c /etc/wpa_supplicant/wpa_supplicant.conf -i wlan0
	#dhclient wlan0
	#sudo ifconfig wlan0 down
	#sudo ifconfig wlan0 up
	#sudo service dnsmasq restart
	#sudo sysv-rc-conf dnsmasq on
	sudo hostapd /etc/hostapd/hostapd.conf
fi


echo -n "Accesspointを停止しますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then

	sudo service hostapd stop
fi


read INPUT_WORD
