#!/bin/bash
set -Cu
echo -n "4Dsystem 4DPi-35 displayのInstallをしますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then

wget http://www.4dsystems.com.au/downloads/4DPi/All/4d-hats_4-4-34_v1.1.tar.gz
sudo tar -xzvf 4d-hats_4-4-34_v1.1.tar.gz -C /
sudo raspi-config
sudo apt-get install xinput-calibrator
sudo apt-get install xserver-xorg-input-evdev
sudo mv /usr/share/X11/xorg.conf.d/10-evdev.conf /usr/share/X11/xorg.conf.d/45-evdev.conf
ls /usr/share/X11/xorg.conf.d

fi

echo -n "Quimat displayのInstallをしますか? (y/n)"‮
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then

cd /home/pi/OneDrive/Linux/ProgramInstall
git clone https://github.com/goodtft/LCD-show.git
chmod -R 755 ./LCD-show/
pwd

#sudo cp ./LCD-show/usr/99-calibration.conf-3508 /etc/X11/xorg.conf.d/99-calibration.conf
sudo cp ./LCD-show/usr/99-calibration.conf-3508 /usr/share/X11/xorg.conf.d/99-calibration.conf

#sudo sed -i -e 's!"SwapAxes"      "1"!"SwapAxes"      "1"\n      Option  "InvertY"      "1"\n      Option  "TransformatinMatrix"      "-1 0 1 0 -1 1 0 0 1"!' /etc/X11/xorg.conf.d/99-calibration.conf
#sudo sed -i -e 's!"SwapAxes"      "1"!"SwapAxes"      "1"\n     Option  "TransformatinMatrix"      "0 -1 1 1 0 0 0 0 1"!' /etc/X11/xorg.conf.d/99-calibration.conf
sudo sed -i -e 's!"SwapAxes"      "1"!"SwapAxes"      "1"\n     Option  "TransformatinMatrix"      "0 -1 1 1 0 0 0 0 1"!' /usr/share/X11/xorg.conf.d/99-calibration.conf
sudo sed -i -e 's!#dtparam=spi=on!dtparam=spi=on\ndtoverlay=ads7846,cs=1,penirq=25,penirq_pull=2,speed=50000,keep_vref_on=0,swapxy=0,pmax=255,xohms=150,xmin=200,xmax=3900,ymin=200,ymax=3900!' /boot/config.txt
sudo apt-get install xserver-xorg-input-evdev
sudo cp -rf /usr/share/X11/xorg.conf.d/10-evdev.conf /usr/share/X11/xorg.conf.d/45-evdev.conf
read INPUT_WORD


fi