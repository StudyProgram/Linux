#!/bin/bash

set -Cu

echo -n "ラズベリーパイとProgram Listの更新をしますか？(y/n) "
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]
then
	sudo rpi-update
	sudo apt-get update
	sudo apt-get upgrade
fi

ChangeJapanese(){
	#日本語化
	#sudo dpkg-reconfigure locales

	#日本語入力
	#sudo apt-get install fonts-vlgothic
	#sudo apt-get install ibus-anthy

	InstallFile="fonts-vlgothic"
	sudo apt-get install $InstallFile
	InstallFile="ibus-anthy"
	sudo apt-get install $InstallFile
}

echo -n "日本語入力のInstallをしますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	ChangeJapanese
fi

echo -n "Partition操作SoftのInstallをしますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	sudo apt-get install gparted
fi


echo -n "Remote操作のInstallをしますか？(y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	sudo mkdir /boot/ssh
	sudo apt-get install xrdp
	sudo apt-get install vnc4server
	#sudo apt-get install tightvncserver
	#tightvncserver
	sudo apt-get install samba
	sudo smbpasswd -a pi

	echo "USB1 読込設定"
	A="#======================= Share Definitions =======================" 
	B="#== Share Definitions ==\n[USB1]\n   comment = RaspberryPI\n   path = /media/pi\n   guest ok = Yes\n   read only = no\n   force user = pi"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/samba/smb.conf


fi

echo -n "SoftKeyboardのInstallをしますか？(y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	sudo apt-get install matchbox-keyboard
	sudo apt-get install xvkbd
fi

echo -n "プログラム関係のInstallをしますか？(y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then

	sudo apt-get install vim
	mkdir ./ProgramInstall
	cd ./ProgramInstall
	sudo wget https://sourceforge.net/projects/webiopi/files/WebIOPi-0.7.1.tar.gz
	sudo tar xvzf WebIOPi-0.7.1.tar.gz
	cd WebIOPi-0.7.1
	wget https://raw.githubusercontent.com/doublebind/raspi/master/webiopi-pi2bplus.patch
	patch -p1 -i webiopi-pi2bplus.patch

	sudo apt-get install python-sklearn
	sudo apt-get install python-pip
	sudo pip install flask

	sudo apt-get install git-cola
	cd ../..
fi

echo -n "access point(hostapd)のInstallをしますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	sudo apt-get install hostapd

	sudo cp /usr/share/doc/hostapd/examples/hostapd.conf.gz /etc/hostapd/
	sudo gzip -d /etc/hostapd/hostapd.conf.gz


	echo "認証アルゴリズム設定"
	A="auth_algs=3" 
	B="auth_algs=1"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "チャンネル設定"
	A="channel=1" 
	B="channel=6"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "ドライバ設定"
	A="# driver=hostap" 
	B="driver=nl80211\n#rtl871xdrv"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "wpa設定"
	A="#wpa=1" 
	B="wpa=2"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "ssid設定"
	read INPUT_WORD
	A="ssid=test" 
	B="ssid=$INPUT_WORD"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "pass phrase設定"
	read INPUT_WORD
	A="#wpa_passphrase=.*" 
	B="wpa_passphrase=$INPUT_WORD"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "鍵管理アルゴリズム設定"
	A="#wpa_key_mgmt=WPA-PSK WPA-EAP" 
	B="wpa_key_mgmt=WPA-PSK"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "WPA Pair wise"
	A="#wpa_pairwise=TKIP CCMP" 
	B="wpa_pairwise=TKIP"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "RSN/WPAの共有鍵暗号化方式"
	A="#rsn_pairwise=CCMP" 
	B="rsn_pairwise=CCMP"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "IEEE 802.11n を有効"
	A="#ieee80211n=1"
	B="ieee80211n=1"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "ht_capab"
	A="#ht_capab=\[HT40-\]\[SHORT-GI-20\]\[SHORT-GI-40\]"
	B="ht_capab=[SHORT-GI-20]"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "Country code設定"
	A="#country_code=US"
	B="country_code=JP"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/hostapd/hostapd.conf

	echo "/etc/default/hostapdの読込みファイル設定"
	A='#DAEMON_CONF=""'
	B='DAEMON_CONF="/etc/hostapd/hostapd.conf"'
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/default/hostapd

	echo "/etc/default/hostapdの読込みファイル設定"
	A='#DAEMON_CONF='
	B='DAEMON_CONF='
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/default/hostapd

	echo "/etc/dhcpcd.confの読込みファイル設定"
	A='#fallback static_eth0'
	B="# fallback static_eth0\nauto br0\n    iface br0 inet dhcp\n    bridge_ports wlan1"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/dhcpcd.conf

fi

echo -n "iptablesを設定しますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	echo "iptable install"
	sudo apt-get install iptables-persistent

	sudo sed -i -e 's!#net.ipv4.ip_forward=1!net.ipv4.ip_forward=1!' /etc/sysctl.conf

	echo "iptable 設定"
	sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
	sudo iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
	sudo iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
	sudo iptables -t nat -A POSTROUTING -o wlan1 -j MASQUERADE
	sudo iptables -A FORWARD -i wlan1 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
	sudo iptables -A FORWARD -i wlan0 -o wlan1 -j ACCEPT

	sudo sh -c "iptables-save > /etc/iptables/ipv4.nat"
	sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
	#sudo sed -i -e 's!0!1!' /proc/sys/net/ipv4/ip_forward
fi


echo -n "DNS server(dnsmasq)のInstallをしますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	sudo apt-get install dnsmasq
	sudo apt-get install dnsutils
	sudo apt-get install sysv-rc-conf 
	sudo service dnsmasq stop
	sudo cp /etc/dnsmasq.conf /etc/dnsmasq.conf.old

	echo "domain-needed設定"
	A="#domain-needed"
	B="domain-needed"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/dnsmasq.conf

	echo "dhcp-range設定"
	A="#dhcp-range=192.168.0.50,192.168.0.150,12h"
	B="dhcp-range=192.168.0.50,192.168.0.150,24h"
	echo "$A -> $B"
	sudo sed -i -e "s!$A!$B!" /etc/dnsmasq.conf

	sudo sed -i -e 's!#bogus-priv!bogus-priv!' /etc/dnsmasq.conf
	sudo sed -i -e 's!#local=/localnet/!local=/localnet/!' /etc/dnsmasq.conf
	sudo sed -i -e 's!#expand-hosts!expand-hosts!' /etc/dnsmasq.conf
	sudo sed -i -e 's!#domain=thekelleys.org.uk!domain=thekelleys.org.uk!' /etc/dnsmasq.conf

	#echo -n "下記例のように書いてください "
	#echo "127.0.0.1     localhost"
	#echo "127.0.1.1     raspberrypi"
	#echo ""
	#echo "192.168.19.2  HOST1"
	#echo "192.168.19.3  HOST2"

	#read INPUT_WORD

	#sudo nano /etc/hosts
	sudo service dnsmasq restart
	sudo sysv-rc-conf dnsmasq on
fi

echo -n "access point(hostapd)を起動しますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then
	sudo ifconfig wlan0 down
	sudo ifconfig wlan0 up
	echo "execute hostapd".
	sudo hostapd /etc/hostapd/hostapd.conf
	sudo systemctl status hostapd
fi



echo -n "Onedriveの設定をしますか? (y/n)"
read INPUT_WORD
if [[ $INPUT_WORD = "y" ]]; then

	sudo apt-get install python3
	mkdir ./ProgramInstall

	cd ./ProgramInstall
	pwd
	git clone https://github.com/xybu/onedrived-dev
	cd ./onedrived-dev

	python3 ./setup.py test
	sudo pip3 install -e.

	onedrived-pref account add
	onedrived-pref drive set
	onedrived start --debug
	cd ../..
	pwd
	read INPUT_WORD
fi

